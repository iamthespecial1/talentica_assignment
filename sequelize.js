const Sequelize = require('sequelize')
const UserModel = require('./model/user')

const sequelize = new Sequelize('devass', 'root', 'root', {
  host: 'localhost',
  dialect: 'mysql',
  pool: {
    max: 10,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
})

const User = UserModel(sequelize, Sequelize)


module.exports = {
  User,
  sequelize
}