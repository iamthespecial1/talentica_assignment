var express = require('express');
var router = express.Router();
const { User } = require('../sequelize')
const fs = require('fs')
const util = require('util')

const readFilePromise = util.promisify(fs.readFile)
/* GET users listing. */
router.get('/', function (req, res, next) {
  User.findAll()
    .then((users) => res.json(users))
    .catch(err => {
      res.send(err)
    })
  // res.send("Hello")
});

router.post('/addUser', function (req, res, next) {
  User.create(req.body)
    .then(user => res.json(user))
})

router.delete('/delUser/:id', function (req, res, next) {
  const id = req.params.id;
  User.destroy({
    where: { id }
  })
    .then(deleted => {
      res.json(deleted);
    });
})

router.get('/readFile', function (req, res, next) {
  readFilePromise('file.txt')
    .then(data => { res.send(data) })
    .catch(err => { res.send(err) })
});

module.exports = router;
